package com.seven.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Size;
import java.time.LocalDate;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "cliente")
public class Cliente {
    @Id
    private String id;

    @Size(min = 3)
    @Field(name = "nombres")
    private String nombres;

    @Field(name = "apellidos")
    private String apellidos;

    @Field(name = "fechaNac")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate fechaNac;

    @Field(name = "urlFoto")
    private String urlFoto;

    @Field(name = "dni")
    private String dni;
}

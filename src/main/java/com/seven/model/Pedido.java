package com.seven.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "pedido")
public class Pedido {

    @Id
    private String id;

    @Field(name = "dniCliente")
    private String dniCliente;

    @Field(name = "nombreCliente")
    private String nombreCliente;

    @Field(name = "mesero")
    private Usuario mesero;

    @Field(name = "cocinero")
    private Usuario cocinero;

    @Field(name = "items")
    private List<PedidoItem> items;

    @Field(name = "descripcion")
    private String descripcion;

    @Field(name = "direccion")
    private String direccion;

    @Field(name = "referencia")
    private String referencia;

    @Field(name = "latitude")
    private String latitude;

    @Field(name = "longitude")
    private String longitude;

    @Field(name = "tipo")
    private Integer tipo;

    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private LocalDateTime creadoEn = LocalDateTime.now();

    @Field(name = "estado")
    private Boolean estado;

    @Field(name = "restaurante")
    private Restaurante restaurante;

    @Field(name = "mesa")
    private Mesa mesa;

    @Field(name = "chofer")
    private Usuario chofer;

    @Field(name = "isEntregado")
    private Boolean isEntregado;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDniCliente() {
        return dniCliente;
    }

    public void setDniCliente(String dniCliente) {
        this.dniCliente = dniCliente;
    }

    public Usuario getMesero() {
        return mesero;
    }

    public void setMesero(Usuario mesero) {
        this.mesero = mesero;
    }

    public Usuario getCocinero() {
        return cocinero;
    }

    public void setCocinero(Usuario cocinero) {
        this.cocinero = cocinero;
    }

    public List<PedidoItem> getItems() {
        return items;
    }

    public void setItems(List<PedidoItem> items) {
        this.items = items;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public LocalDateTime getCreadoEn() {
        return creadoEn;
    }

    public void setCreadoEn(LocalDateTime creadoEn) {
        this.creadoEn = creadoEn;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Restaurante getRestaurante() {
        return restaurante;
    }

    public void setRestaurante(Restaurante restaurante) {
        this.restaurante = restaurante;
    }

    public Mesa getMesa() {
        return mesa;
    }

    public void setMesa(Mesa mesa) {
        this.mesa = mesa;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Usuario getChofer() {
        return chofer;
    }

    public void setChofer(Usuario chofer) {
        this.chofer = chofer;
    }

    public Boolean getEntregado() {
        return isEntregado;
    }

    public void setEntregado(Boolean entregado) {
        isEntregado = entregado;
    }
}

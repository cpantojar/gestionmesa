package com.seven.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;


@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "preparacion")
public class Preparacion {

    @Id
    private String id;
    private Producto producto;

    @Field(name = "diaPreparacion")
    @JsonFormat(pattern="dd/MM/yyyy")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate diaPreparacion;

    private Integer cantidad;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public LocalDate getDiaPreparacion() {
        return diaPreparacion;
    }

    public void setDiaPreparacion(LocalDate diaPreparacion) {
        this.diaPreparacion = diaPreparacion;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

}

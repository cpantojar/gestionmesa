package com.seven.controller;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.seven.model.Restaurante;
import com.seven.service.RestauranteService;
import org.cloudinary.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.util.Map;

@RestController
@RequestMapping("/api/restaurante")
public class RestauranteController {

    @Autowired
    private RestauranteService restauranteService;

    @GetMapping("/listar")
    public Flux<Restaurante> listar() {
        return restauranteService.listar();
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<Restaurante>> listarPorId(@PathVariable("id") String id) {
        return restauranteService.listarPorId(id)                //Mono<Restaurante>
                .map(p -> ResponseEntity.ok()  //Mono<ResponseEntity>
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(p)
                )
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping("/registrar")
    public Mono<ResponseEntity<Restaurante>> registrar(@Valid @RequestBody Restaurante restaurante, final ServerHttpRequest req) {
        return restauranteService.registrar(restaurante)
                .map(p -> ResponseEntity.created(URI.create(req.getURI().toString().concat(p.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(p)
                );
    }

    @PutMapping("/modificar/{id}")
    public Mono<ResponseEntity<Restaurante>> modificar(@Valid @RequestBody Restaurante restaurante,
                                                       @PathVariable("id") String id) {

        Mono<Restaurante> monoRestauranteRequest = Mono.just(restaurante);
        Mono<Restaurante> monoRestauranteBD = restauranteService.listarPorId(id);

        return monoRestauranteBD
                .zipWith(monoRestauranteRequest, (bd, request) -> {
                    bd.setId(id);
                    if (request.getNombre() != null)
                        bd.setNombre(request.getNombre());
                    if (request.getRuc() != null)
                        bd.setRuc(request.getRuc());
                    if (request.getEstado() != null)
                        bd.setEstado(request.getEstado());
                    if (request.getAforo() != null)
                        bd.setAforo(request.getAforo());
                    if(request.getCodigo() != null)
                        bd.setCodigo(request.getCodigo());
                    return bd;
                })
                .flatMap(restauranteService::modificar) //x -> service.modificar(x)
                .map(pl -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(pl))
                .defaultIfEmpty(new ResponseEntity<Restaurante>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/subir/{id}")
    public Mono<ResponseEntity<Restaurante>> subir(@PathVariable String id,
                                                   @RequestPart FilePart file) throws IOException {

        Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
                "cloud_name", "dapx1we5u",
                "api_key", "588174139831725",
                "api_secret", "BWobadpBwsshUI2oKyfKOAUqg5E"));
        File filePhoto = Files.createTempFile("temp", file.filename()).toFile();
        return  file.transferTo(filePhoto)
                .then(restauranteService.listarPorId(id)
                        .flatMap(c -> {
                            Map response;
                            try {
                                response = cloudinary.uploader().upload(filePhoto , ObjectUtils.asMap("resource_type", "auto"));

                                JSONObject json=new JSONObject(response);
                                String url=json.getString("url");

                                c.setUrlFoto(url);
                                //System.out.println(url);

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            return restauranteService.modificar(c).then(Mono.just(ResponseEntity.ok().body(c)));
                        })
                        .defaultIfEmpty(ResponseEntity.notFound().build())
                );

    }


}

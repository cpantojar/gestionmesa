package com.seven.controller;

import com.seven.model.Categoria;
import com.seven.model.Producto;
import com.seven.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/api/producto")
public class ProductoController {
    @Autowired
    private ProductoService productoService;

    @GetMapping("/listar")
    public Flux<Producto> listar() {
        return productoService.listar();
    }

    @GetMapping("/{codigoRestaurante}/listar")
    public Flux<Producto> listarByRestauranteCode(@PathVariable("codigoRestaurante") String codigoRestaurante) {
        return productoService.getProductoByRestauranteCode(codigoRestaurante);
    }

    @PostMapping("/registrar")
    public Mono<ResponseEntity<Producto>> registrar(@Valid @RequestBody Producto producto, final ServerHttpRequest req) {
        return productoService.registrar(producto)
                .map(p -> ResponseEntity.created(URI.create(req.getURI().toString().concat(p.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(p)
                );
    }

    @PutMapping("/modificar/{id}")
    public Mono<ResponseEntity<Producto>> modificar(@Valid @RequestBody Producto producto,
                                                    @PathVariable("id") String id) {

        Mono<Producto> monoProductoRequest = Mono.just(producto);
        Mono<Producto> monoProductoBD = productoService.listarPorId(id);

        return monoProductoBD
                .zipWith(monoProductoRequest, (bd, request) -> {
                    bd.setId(id);
                    if (request.getEstado() != null)
                        bd.setEstado(request.getEstado());
                    if (request.getNombre() != null)
                        bd.setNombre(request.getNombre());
                    if (request.getEstado() != null)
                        bd.setEstado(request.getEstado());
                    if(request.getCategoria() != null)
                        bd.setCategoria(request.getCategoria());

                    return bd;
                })
                .flatMap(productoService::modificar) //x -> service.modificar(x)
                .map(pl -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(pl))
                .defaultIfEmpty(new ResponseEntity<Producto>(HttpStatus.NOT_FOUND));
    }
}

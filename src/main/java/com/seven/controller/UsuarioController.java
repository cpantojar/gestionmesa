package com.seven.controller;

import com.seven.model.Restaurante;
import com.seven.model.Usuario;
import com.seven.security.AuthRequest;
import com.seven.security.AuthResponse;
import com.seven.security.JWTUtil;
import com.seven.security.response.ErrorLogin;
import com.seven.service.RestauranteService;
import com.seven.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.net.URI;
import java.util.*;

@Validated
@RestController
@RequestMapping("/api/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JWTUtil jwtUtil;

    @Autowired
    private RestauranteService restauranteService;

    @PostMapping("/{restaraunteCode}/registrar")
    public Mono<ResponseEntity<Usuario>> registrar(@PathVariable("restaraunteCode") String restaraunteCode,
                                                   @Valid @RequestBody Usuario usuario,
                                                   final ServerHttpRequest req) {

        return restauranteService.findRestauranteByCodigo(restaraunteCode)
                .map(p -> {
                    Restaurante restaurante = new Restaurante();
                    restaurante.setCodigo(p.getCodigo());
                    restaurante.setId(p.getId());
                    restaurante.setRuc(p.getRuc());
                    usuario.setRestaurante(restaurante);
                    return usuario;
                }).flatMap(u -> usuarioService.findUsuarioByMail(usuario.getMail()))
                .defaultIfEmpty(usuario)
                .flatMap(result -> {
                    if (result.getId() == null) {
                        result.setClave(passwordEncoder.encode(result.getDNI()));
                        result.setEstado(false);
                        return Mono.just(result);
                    } else {
                        return Mono.error(new ResponseStatusException(HttpStatus.BAD_REQUEST, "Mail ingresado ya existe"));
                    }
                })
                .flatMap(usuarioService::registrar)
                .map(p -> ResponseEntity.created(URI.create(req.getURI().toString().concat(p.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(p)
                );
    }

    @GetMapping("/cambiar_estado/{id}")
    public Mono<ResponseEntity<Usuario>> cambiarEstado(@PathVariable("id") String id) {

        return usuarioService.listarPorId(id)
                .flatMap(result -> {
                    if (result.getId() != null) {
                        result.setEstado(!result.getEstado());
                        return Mono.just(result);
                    } else {
                        return Mono.error(new ResponseStatusException(HttpStatus.BAD_REQUEST, "Usuario ingresado no existe en nuestra base de datos"));
                    }
                })
                .flatMap(usuarioService::modificar)
                .map(usuario -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(usuario));
    }


    @PostMapping("/login")
    public Mono<ResponseEntity<?>> login(@RequestBody AuthRequest request) {

        return usuarioService.buscarPorMail(request.getMail())
                .map((userDetails) -> {
                    if (BCrypt.checkpw(request.getPassword(), userDetails.getPassword())) {
                        if (userDetails.isEnabled()) {
                            String token = jwtUtil.generateToken(userDetails);
                            Date expiration = jwtUtil.getExpirationDateFromToken(token);
                            //The return of login response
                            return ResponseEntity.ok(new AuthResponse(token, expiration));
                        } else {
                            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new ErrorLogin("Usuario Bloqueado", new Date()));
                        }

                    } else {
                        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new ErrorLogin("Credenciales Incorrectas", new Date()));
                    }
                }).defaultIfEmpty(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build());
    }

    @PostMapping("/change_password")
    public Mono<ResponseEntity<?>> changePassword(@RequestBody Map<String, String> json) {
        return usuarioService.listarPorId(json.get("id"))
                .flatMap(result -> {
                    if (result.getId() != null) {
                        if (BCrypt.checkpw(json.get("oldPassword"), result.getClave())) {
                            result.setClave(passwordEncoder.encode(json.get("newPassword")));
                            return Mono.just(result);
                        } else {
                            return Mono.error(new ResponseStatusException(HttpStatus.BAD_REQUEST, "Credenciales incorrectas"));
                        }
                    } else {
                        return Mono.error(new ResponseStatusException(HttpStatus.BAD_REQUEST, "Usuario ingresado no existe en nuestra base de datos"));
                    }
                })
                .flatMap(usuarioService::modificar)
                .map(usuario -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(usuario));

    }

    @GetMapping("/{restaraunteCode}/listar")
    public Flux<Usuario> listar(@PathVariable("restaraunteCode") String restaraunteCode) {
        return usuarioService.findUsuarioByRestaurante_Codigo(restaraunteCode);
    }

    @PostMapping("/editar/{id}")
    public Mono<ResponseEntity<Usuario>> editar(@Valid @RequestBody Usuario usuario,
                                                @PathVariable("id") String id,
                                                final ServerHttpRequest req) {
        return usuarioService.listarPorId(id)
                .flatMap(result -> {
                    if (result.getId() != null) {
                        if (usuario.getApellidoMaterno() != null)
                            result.setApellidoMaterno(usuario.getApellidoMaterno());
                        if (usuario.getApellidoPaterno() != null)
                            result.setApellidoPaterno(usuario.getApellidoPaterno());
                        if (usuario.getDNI() != null)
                            result.setDNI(usuario.getDNI());
                        if (usuario.getNombre() != null)
                            result.setNombre(usuario.getNombre());
                        if (usuario.getUsername() != null)
                            result.setUsername(usuario.getUsername());
                        if (usuario.getRoles() != null)
                            result.setRoles(usuario.getRoles());
                        if (usuario.getFoto() != null)
                            result.setFoto(usuario.getFoto());
                        return Mono.just(result);
                    } else {
                        return Mono.error(new ResponseStatusException(HttpStatus.BAD_REQUEST, "Usuario ingresado no existe en nuestra base de datos"));
                    }
                })
                .flatMap(usuarioService::modificar)
                .map(x -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(x));
    }
}
package com.seven.controller;

import com.seven.model.Preparacion;
import com.seven.model.Producto;
import com.seven.service.PreparacionService;
import com.seven.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/api/preparacion")
public class PreparacionController {

    @Autowired
    private PreparacionService preparacionService;

    @Autowired
    private ProductoService productoService;

    @GetMapping("/listar")
    public Flux<Preparacion> listar() {
        return preparacionService.listar();
    }

    @PostMapping("/registrar")
    public Mono<ResponseEntity<Preparacion>> registrar(@Valid @RequestBody Preparacion preparacion, final ServerHttpRequest req) {
        
        return productoService.listarPorId(preparacion.getProducto().getId())
                .flatMap(producto -> {
                    producto.setCantidad(preparacion.getCantidad());
                    return Mono.just(producto);
                })
                //tengo que evaluar si el mismo dia se preparo el plato
                // si se preparo el mismo plato, modificar cantidad, agregar
                // si es nueva preparacion del dia, agregar preparacion nueva
                .flatMap(productoService::modificar)
                .flatMap(x -> {
                    Preparacion pr = preparacion;
                    Producto prod = new Producto();
                    prod.setId(x.getId());
                    pr.setProducto(prod);
                    return preparacionService.registrar(pr);
                })
                .map(p -> ResponseEntity.created(URI.create(req.getURI().toString().concat(p.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(p)
                );
    }
}

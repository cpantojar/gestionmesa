package com.seven.controller;

import com.seven.model.Restaurante;
import com.seven.model.Rol;
import com.seven.service.RolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;

@RestController
@RequestMapping("/api/seguridad")
public class SecurityController {
    @Autowired
    private RolService rolService;

    @PostMapping("/rol/insertar")
    public Mono<ResponseEntity<Rol>> registrar(@RequestBody Rol rol, final ServerHttpRequest req) {
        return rolService.registrar(rol)
                .map(p -> ResponseEntity.created(URI.create(req.getURI().toString().concat(p.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(p)
                );
    }

    @GetMapping("rol/listar")
    public Flux<Rol> listar() {
        return rolService.listar();
    }
}

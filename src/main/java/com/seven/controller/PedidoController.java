package com.seven.controller;

import com.seven.model.Pedido;
import com.seven.model.Restaurante;
import com.seven.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/pedido")
public class PedidoController {

    @Autowired
    private RestauranteService restauranteService;

    @Autowired
    private PedidoService pedidoService;

    @Autowired
    private MesaService mesaService;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private ProductoService productoService;

    @GetMapping("/{restaraunteCode}/listar")
    public Flux<Pedido> listar(@PathVariable("restaraunteCode") String restaraunteCode) {
        return pedidoService.findPedidoByRestaurante_Codigo(restaraunteCode)
                .flatMap(pedido -> {
                    if (pedido.getMesa() != null) {
                        return Mono.just(pedido)
                                .zipWith(mesaService.listarPorId(pedido.getMesa().getId()), (pe, me) -> {
                                    pe.setMesa(me);
                                    return pe;
                                });
                    } else {
                        return Mono.just(pedido);
                    }

                }).flatMap(pedido -> {
                    if (pedido.getChofer() != null) {
                        return Mono.just(pedido)
                                .zipWith(usuarioService.listarPorId(pedido.getChofer().getId()), (pe, u) -> {
                                    pe.setChofer(u);
                                    return pe;
                                });
                    } else {
                        return Mono.just(pedido);
                    }

                })
                .flatMap(pedido -> {
                    return Flux.fromIterable(pedido.getItems()).flatMap(item -> {
                        return productoService.listarPorId(item.getProducto().getId())
                                .map(producto -> {
                                    item.setProducto(producto);
                                    return item;
                                });
                    }).collectList().flatMap(pedidoItems -> {
                        pedido.setItems(pedidoItems);
                        return Mono.just(pedido);
                    });
                });
    }

    @GetMapping("/get-by-id/{id}")
    public Mono<ResponseEntity<Pedido>> listarById(@PathVariable("id") String id) {
        return pedidoService.listarPorId(id)                //Mono<Mesa>
                .map(p -> ResponseEntity.ok()  //Mono<ResponseEntity>
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(p)
                )
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping("/{restaraunteCode}/registrar")
    public Mono<ResponseEntity<Pedido>> registrar(@PathVariable("restaraunteCode") String restaraunteCode,
                                                  @Valid @RequestBody Pedido pedido,
                                                  final ServerHttpRequest req) {

        return restauranteService.findRestauranteByCodigo(restaraunteCode)
                .map(p -> {
                    Restaurante restaurante = new Restaurante();
                    restaurante.setCodigo(p.getCodigo());
                    restaurante.setId(p.getId());
                    restaurante.setRuc(p.getRuc());
                    pedido.setRestaurante(restaurante);
                    pedido.setEstado(false);
                    pedido.setEntregado(false);
                    return pedido;
                })
                .flatMap(pedidoService::registrar)
                .flatMap(p -> {
                    if (p.getMesa() != null) {
                        return Mono.just(p)
                                .zipWith(mesaService.listarPorId(p.getMesa().getId())
                                        .map(mesa -> {
                                            mesa.setEstado(2);
                                            return mesa;
                                        }).flatMap(mesaService::modificar), (pe, me) -> {
                                    pe.setMesa(me);
                                    return pe;
                                });
                    } else {
                        return Mono.just(p);
                    }

                })
                .map(p -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(p)
                )
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping("/get-by-mesa/{mesa-id}")
    public Mono<ResponseEntity<Pedido>> getByMesa(@PathVariable("mesa-id") String id) {
        return pedidoService.findPedidoByMesaIDAndEstadoFalse(id)
                .flatMap(pedido -> {
                    return Flux.fromIterable(pedido.getItems()).flatMap(producto -> {
                        return productoService.listarPorId(producto.getProducto().getId())
                                .map(x -> {
                                    producto.setProducto(x);
                                    return producto;
                                });
                    }).collectList().flatMap(itemList -> {
                        pedido.setItems(itemList);
                        return Mono.just(pedido);
                    });
                })
                .map(p -> ResponseEntity.ok()  //Mono<ResponseEntity>
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(p)
                )
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PutMapping("/modificar/{id}")
    public Mono<ResponseEntity<Pedido>> modificar(@Valid @RequestBody Pedido pedido,
                                                  @PathVariable("id") String id) {

        Mono<Pedido> monoPedidoRequest = Mono.just(pedido);
        Mono<Pedido> monoPedidoBD = pedidoService.listarPorId(id);

        return monoPedidoBD
                .zipWith(monoPedidoRequest, (bd, request) -> {
                    bd.setId(id);
                    if(request.getDireccion() != null)
                        bd.setDireccion(request.getDireccion());
                    if(request.getReferencia() != null)
                        bd.setReferencia(request.getReferencia());
                    if(request.getChofer() != null && request.getChofer().getId() != null)
                        bd.setChofer(request.getChofer());
                    if (request.getEstado() != null)
                        bd.setEstado(request.getEstado());
                    if (request.getItems() != null && !request.getItems().isEmpty())
                        bd.setItems(request.getItems());
                    if (request.getNombreCliente() != null)
                        bd.setNombreCliente(request.getNombreCliente());
                    return bd;
                })
                .flatMap(pedidoService::modificar) //x -> service.modificar(x)
                .map(pl -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(pl))
                .defaultIfEmpty(new ResponseEntity<Pedido>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/cambiar_estado/{id}")
    public Mono<ResponseEntity<Pedido>> cambiarEstado(@PathVariable("id") String id) {

        return pedidoService.listarPorId(id)
                .flatMap(result -> {
                    if (result.getMesa() != null) {
                        return Mono.just(result).zipWith(mesaService.listarPorId(result.getMesa().getId())
                                .map(mesa -> {
                                    mesa.setEstado(1);
                                    return mesa;
                                }).flatMap(mesaService::modificar), (pe, mesa) -> {
                            pe.setEstado(true);
                            return pe;
                        });
                    }else {
                        result.setEstado(true);
                        return Mono.just(result);
                    }


                })
                .flatMap(pedidoService::modificar)
                .map(pedido -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(pedido));
    }

    @GetMapping("/cambiar_entregado/{id}")
    public Mono<ResponseEntity<Pedido>> entregar(@PathVariable("id") String id) {

        return pedidoService.listarPorId(id)
                .flatMap(result -> {
                    if (result.getId() != null) {
                        result.setEntregado(!result.getEntregado());
                        return Mono.just(result);
                    } else {
                        return Mono.error(new ResponseStatusException(HttpStatus.BAD_REQUEST, "El pedido ingresado no existe en nuestra base de datos"));
                    }
                })
                .flatMap(pedidoService::modificar)
                .map(pedido -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(pedido));
    }


}

package com.seven.controller;

import com.seven.model.Mesa;
import com.seven.model.Restaurante;
import com.seven.service.MesaService;
import com.seven.service.RestauranteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/api/mesa")
public class MesaController {
    @Autowired
    private MesaService mesaService;

    @Autowired
    private RestauranteService restauranteService;

    @GetMapping("/listar")
    public Flux<Mesa> listar() {
        return mesaService.listar();
    }

    @GetMapping("/{codigoRestaurante}/listar")
    public Flux<Mesa> listarByRestaurante(@PathVariable("codigoRestaurante") String codigoRestaurante) {
        return mesaService.listarMesaByRestaurantCodigo(codigoRestaurante);
    }

    @GetMapping("/actualizar/disponibilidad/{id}")
    public Mono<ResponseEntity<Mesa>> actualizarDisponibilidad(@PathVariable("id") String id) {
        Mono<Mesa> monoRestauranteBD = mesaService.listarPorId(id);
        return monoRestauranteBD
                .map(mesa -> {
                    if (mesa.getHabilitado()) {
                        mesa.setHabilitado(false);
                    } else {
                        mesa.setHabilitado(true);
                    }
                    return mesa;
                })
                .flatMap(mesaService::modificar) //x -> service.modificar(x)
                .map(pl -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(pl))
                .defaultIfEmpty(new ResponseEntity<Mesa>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/actualizar/estado/{id}/{estado}")
    public Mono<ResponseEntity<Mesa>> actualizarEstado(@PathVariable("id") String id,
                                                       @PathVariable("estado") Integer estado) {
        Mono<Mesa> monoRestauranteBD = mesaService.listarPorId(id);
        return monoRestauranteBD
                .map(mesa -> {
                    mesa.setEstado(estado);
                    return mesa;
                })
                .flatMap(mesaService::modificar) //x -> service.modificar(x)
                .map(pl -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(pl))
                .defaultIfEmpty(new ResponseEntity<Mesa>(HttpStatus.NOT_FOUND));
    }


    @GetMapping("/{id}")
    public Mono<ResponseEntity<Mesa>> listarPorId(@PathVariable("id") String id) {
        return mesaService.listarPorId(id)                //Mono<Mesa>
                .map(p -> ResponseEntity.ok()  //Mono<ResponseEntity>
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(p)
                )
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping("/{codigoRestaurante}/registrar")
    public Mono<ResponseEntity<Mesa>> registrar(
            @PathVariable("codigoRestaurante") String codigoRestaurante,
            @Valid @RequestBody Mesa mesa,
            final ServerHttpRequest req) {

        return restauranteService.findRestauranteByCodigo(codigoRestaurante)
                .map(p -> {
                    Restaurante restaurante = new Restaurante();
                    restaurante.setCodigo(p.getCodigo());
                    restaurante.setId(p.getId());
                    restaurante.setRuc(p.getRuc());
                    mesa.setRestaurante(restaurante);
                    return mesa;
                })
                .flatMap(mesaService::registrar)
                .map(p -> ResponseEntity.created(URI.create(req.getURI().toString().concat(p.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(p)
                )
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PutMapping("/modificar/{id}")
    public Mono<ResponseEntity<Mesa>> modificar(@Valid @RequestBody Mesa mesa,
                                                @PathVariable("id") String id) {

        Mono<Mesa> monoMesaRequest = Mono.just(mesa);
        Mono<Mesa> monoMesaBD = mesaService.listarPorId(id);

        return monoMesaBD
                .zipWith(monoMesaRequest, (bd, request) -> {
                    bd.setId(id);
                    if (request.getEstado() != null)
                        bd.setEstado(request.getEstado());
                    if (request.getHabilitado() != null)
                        bd.setHabilitado(request.getHabilitado());
                    if (request.getCapacidad() != null)
                        bd.setCapacidad(request.getCapacidad());
                    if (request.getRestaurante() != null)
                        bd.setRestaurante(request.getRestaurante());
                    if (request.getIdentificador() != null)
                        bd.setIdentificador(request.getIdentificador());
                    return bd;
                })
                .flatMap(mesaService::modificar) //x -> service.modificar(x)
                .map(pl -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(pl))
                .defaultIfEmpty(new ResponseEntity<Mesa>(HttpStatus.NOT_FOUND));
    }
}

package com.seven.controller;

import com.seven.model.Categoria;
import com.seven.model.Restaurante;
import com.seven.service.CategoriaService;
import com.seven.service.RestauranteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/api/categoria")
public class CategoriaController {

    @Autowired
    private CategoriaService categoriaService;

    @Autowired
    private RestauranteService restauranteService;

    @GetMapping("/{restaraunteCode}/listar")
    public Flux<Categoria> listar(@PathVariable("restaraunteCode") String restaraunteCode) {
        return categoriaService.listarByRestauranteCodigo(restaraunteCode);
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<Categoria>> listarPorId(@PathVariable("id") String id) {
        return categoriaService.listarPorId(id)                //Mono<Mesa>
                .map(p -> ResponseEntity.ok()  //Mono<ResponseEntity>
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(p)
                )
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping("/{restaraunteCode}/registrar")
    public Mono<ResponseEntity<Categoria>> registrar(@PathVariable("restaraunteCode") String restaraunteCode,
                                                     @Valid @RequestBody Categoria categoria,
                                                     final ServerHttpRequest req) {

        return restauranteService.findRestauranteByCodigo(restaraunteCode)
                .map(p -> {
                    Restaurante restaurante = new Restaurante();
                    restaurante.setCodigo(p.getCodigo());
                    restaurante.setId(p.getId());
                    restaurante.setRuc(p.getRuc());
                    categoria.setRestaurante(restaurante);
                    return categoria;
                }).flatMap(categoriaService::registrar).map(p -> ResponseEntity.created(URI.create(req.getURI().toString().concat(p.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(p)
                )
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PutMapping("/modificar/{id}")
    public Mono<ResponseEntity<Categoria>> modificar(@Valid @RequestBody Categoria categoria,
                                                     @PathVariable("id") String id) {

        Mono<Categoria> monoCategoriaRequest = Mono.just(categoria);
        Mono<Categoria> monoCategoriaBD = categoriaService.listarPorId(id);

        return monoCategoriaBD
                .zipWith(monoCategoriaRequest, (bd, request) -> {
                    bd.setId(id);
                    if (request.getEstado() != null)
                        bd.setEstado(request.getEstado());
                    if (request.getNombre() != null)
                        bd.setNombre(request.getNombre());
                    return bd;
                })
                .flatMap(categoriaService::modificar) //x -> service.modificar(x)
                .map(pl -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(pl))
                .defaultIfEmpty(new ResponseEntity<Categoria>(HttpStatus.NOT_FOUND));
    }
}

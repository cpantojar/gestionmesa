package com.seven.service;

import com.seven.model.Mesa;
import reactor.core.publisher.Flux;

public interface MesaService extends GenericCrud<Mesa,String>{
    Flux<Mesa> listarMesaByRestaurant(String id);
    Flux<Mesa> listarMesaByRestaurantCodigo(String codigoRestaurante);
}

package com.seven.service;

import com.seven.model.Restaurante;
import reactor.core.publisher.Mono;

public interface RestauranteService extends GenericCrud<Restaurante, String> {
    Mono<Restaurante> findRestauranteByCodigo(String code);
}

package com.seven.service;

import com.seven.model.Pedido;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface PedidoService extends  GenericCrud<Pedido,String> {
    Flux<Pedido> findPedidoByRestaurante_Codigo(String codigo);
    Flux<Pedido> findPedidoByRestaurante_Ruc(String ruc);
    Flux<Pedido> findPedidoByRestaurante_Id(String id);
    Mono<Pedido> findPedidoByMesaAndRestauranteFalse(String mesaId, String restauranteId);
    Mono<Pedido> findPedidoByMesaIDAndEstadoFalse(String mesaId);
}

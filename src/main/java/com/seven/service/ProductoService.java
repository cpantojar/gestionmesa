package com.seven.service;


import com.seven.model.Producto;
import reactor.core.publisher.Flux;

public interface ProductoService extends GenericCrud<Producto,String>{
    Flux<Producto> getProductoByRestauranteCode(String code);
}

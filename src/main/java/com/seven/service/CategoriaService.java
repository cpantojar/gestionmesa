package com.seven.service;


import com.seven.model.Categoria;
import com.seven.model.Restaurante;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CategoriaService extends GenericCrud<Categoria,String>{
    Flux<Categoria> listarByRestauranteCodigo(String code);
    Flux<Categoria> ListarByRestauranteRuc(String ruc);
    Flux<Categoria> ListarByRestauranteId(String id);
}

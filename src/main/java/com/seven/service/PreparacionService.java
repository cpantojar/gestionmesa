package com.seven.service;

import com.seven.model.Preparacion;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

public interface  PreparacionService extends GenericCrud<Preparacion,String> {
    Flux<Preparacion> listarPreparacionByProducto(String id);
    Mono<Preparacion> getPreparacionByProductoAndDate(String id, LocalDate fechaPreaparacion);
}

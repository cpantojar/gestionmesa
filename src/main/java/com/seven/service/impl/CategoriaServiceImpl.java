package com.seven.service.impl;


import com.seven.model.Categoria;
import com.seven.repo.CategoriaRepo;
import com.seven.repo.IGenericRepo;
import com.seven.service.CategoriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CategoriaServiceImpl extends GenericCrudImpl<Categoria, String> implements CategoriaService {
    @Autowired
    private CategoriaRepo categoriaRepo;

    @Override
    protected IGenericRepo<Categoria, String> getRepo() {
        return categoriaRepo;
    }

    @Override
    public Flux<Categoria> listarByRestauranteCodigo(String code) {
        return categoriaRepo.findCategoriasByRestaurante_Codigo(code);
    }

    @Override
    public Flux<Categoria> ListarByRestauranteRuc(String ruc) {
        return categoriaRepo.findCategoriasByRestaurante_Ruc(ruc);
    }

    @Override
    public Flux<Categoria> ListarByRestauranteId(String id) {
        return categoriaRepo.findCategoriasByRestaurante_Id(id);
    }
}

package com.seven.service.impl;

import com.seven.model.Rol;
import com.seven.repo.IGenericRepo;
import com.seven.repo.RolRepo;
import com.seven.service.GenericCrud;
import com.seven.service.RolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RolServiceImpl extends GenericCrudImpl<Rol, String> implements RolService {

    @Autowired
    private RolRepo rolRepo;

    @Override
    protected IGenericRepo<Rol, String> getRepo() {
        return rolRepo;
    }
}

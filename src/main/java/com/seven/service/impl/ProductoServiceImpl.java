package com.seven.service.impl;


import com.seven.model.Producto;
import com.seven.repo.IGenericRepo;
import com.seven.repo.ProductoRepo;
import com.seven.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class ProductoServiceImpl extends GenericCrudImpl<Producto, String> implements ProductoService {
    @Autowired
    private ProductoRepo productoRepo;

    @Override
    protected IGenericRepo<Producto, String> getRepo() {
        return productoRepo;
    }

    @Override
    public Flux<Producto> getProductoByRestauranteCode(String code) {
        return productoRepo.findProductoByCategoria_Restaurante_Codigo(code);
    }
}

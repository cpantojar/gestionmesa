package com.seven.service.impl;

import com.seven.model.Mesa;
import com.seven.repo.IGenericRepo;
import com.seven.repo.MesaRepo;
import com.seven.service.MesaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class MesaServiceImpl extends GenericCrudImpl<Mesa, String> implements MesaService {
    @Autowired
    private MesaRepo mesaRepo;

    @Override
    protected IGenericRepo<Mesa, String> getRepo() {
        return mesaRepo;
    }

    @Override
    public Flux<Mesa> listarMesaByRestaurant(String id) {
        return mesaRepo.findMesaByRestaurante_Id(id);
    }

    @Override
    public Flux<Mesa> listarMesaByRestaurantCodigo(String codigoRestaurante) {
        return mesaRepo.findMesaByRestaurante_Codigo(codigoRestaurante);
    }
}

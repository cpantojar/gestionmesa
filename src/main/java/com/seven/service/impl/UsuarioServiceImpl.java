package com.seven.service.impl;

import com.seven.model.Usuario;
import com.seven.repo.IGenericRepo;
import com.seven.repo.RolRepo;
import com.seven.repo.UsuarioRepo;
import com.seven.security.User;
import com.seven.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsuarioServiceImpl extends GenericCrudImpl<Usuario, String> implements UsuarioService {

    @Autowired
    private UsuarioRepo usuarioRepo;

    @Autowired
    private RolRepo rolRepo;

    @Override
    protected IGenericRepo<Usuario, String> getRepo() {
        return usuarioRepo;
    }


    @Override
    public Mono<User> buscarPorMail(String mail) {

        Mono<Usuario> usuarioMono = usuarioRepo.findUsuarioByMail(mail);

        List<String> roles = new ArrayList<String>();


        return usuarioMono.flatMap(user -> {
            return Flux.fromIterable(user.getRoles())
                    .flatMap(rol -> {
                        return rolRepo.findById(rol.getId())
                                .map(r -> {
                                    roles.add(r.getNombre());
                                    return r;
                                });
                    }).collectList().flatMap(lista -> {
                        user.setRoles(lista);
                        return Mono.just(user);
                    });
        }).flatMap(user -> {
            return Mono.just(new User(user.getNombre() + " " + user.getApellidoPaterno() + " " + user.getApellidoMaterno(),
                    user.getUsername(),
                    user.getMail(),
                    user.getClave(),
                    user.getEstado(),
                    roles, user.getRestaurante()));
        });
    }

    @Override
    public Mono<Usuario> findUsuarioByMail(String mail) {
        return usuarioRepo.findUsuarioByMail(mail);
    }

    @Override
    public Flux<Usuario> findUsuarioByRestaurante_Codigo(String codigo) {
        return usuarioRepo.findUsuarioByRestaurante_Codigo(codigo);
    }

    @Override
    public Flux<Usuario> findUsuarioByRestaurante_Ruc(String ruc) {
        return usuarioRepo.findUsuarioByRestaurante_Ruc(ruc);
    }

    @Override
    public Flux<Usuario> findUsuarioByRestaurante_Id(String id) {
        return usuarioRepo.findUsuarioByRestaurante_Id(id);
    }

}

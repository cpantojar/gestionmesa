package com.seven.service.impl;

import com.seven.model.Restaurante;
import com.seven.repo.IGenericRepo;
import com.seven.repo.RestauranteRepo;
import com.seven.service.RestauranteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class RestauranteServiceImpl extends GenericCrudImpl<Restaurante, String> implements RestauranteService {

    @Autowired
    private RestauranteRepo restauranteRepo;

    @Override
    protected IGenericRepo<Restaurante, String> getRepo() {
        return restauranteRepo;
    }

    @Override
    public Mono<Restaurante> findRestauranteByCodigo(String code) {
        return restauranteRepo.findRestauranteByCodigo(code);
    }
}

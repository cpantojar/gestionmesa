package com.seven.service.impl;

import com.seven.model.Preparacion;
import com.seven.repo.IGenericRepo;
import com.seven.repo.PreparacionRepo;
import com.seven.service.PreparacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@Service
public class PreparacionServiceImpl extends GenericCrudImpl<Preparacion, String> implements PreparacionService {

    @Autowired
    private PreparacionRepo preparacionRepo;

    @Override
    protected IGenericRepo<Preparacion, String> getRepo() {
        return preparacionRepo;
    }

    @Override
    public Flux<Preparacion> listarPreparacionByProducto(String id) {
        return preparacionRepo.findPreparacionByProducto(id);
    }

    @Override
    public Mono<Preparacion> getPreparacionByProductoAndDate(String id, LocalDate fechaPreaparacion) {
        return preparacionRepo.findPreparacionByProductoAndDiaPreparacion(id, fechaPreaparacion);
    }


}

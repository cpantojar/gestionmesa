package com.seven.service.impl;

import com.seven.model.Pedido;
import com.seven.repo.IGenericRepo;
import com.seven.repo.PedidoRepo;
import com.seven.service.PedidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class PedidoServiceImpl extends GenericCrudImpl<Pedido, String> implements PedidoService {

    @Autowired
    private PedidoRepo pedidoRepo;

    @Override
    public Flux<Pedido> findPedidoByRestaurante_Codigo(String codigo) {
        return pedidoRepo.findPedidoByRestaurante_Codigo(codigo);
    }

    @Override
    public Flux<Pedido> findPedidoByRestaurante_Ruc(String ruc) {
        return pedidoRepo.findPedidoByRestaurante_Ruc(ruc);
    }

    @Override
    public Flux<Pedido> findPedidoByRestaurante_Id(String id) {
        return pedidoRepo.findPedidoByRestaurante_Id(id);
    }

    @Override
    public Mono<Pedido> findPedidoByMesaAndRestauranteFalse(String mesaId, String restauranteId) {
        return pedidoRepo.findPedidoByMesa_IdAndRestaurante_IdAndEstadoIsFalse(mesaId, restauranteId);
    }

    @Override
    public Mono<Pedido> findPedidoByMesaIDAndEstadoFalse(String mesaId) {
        return pedidoRepo.findPedidoByMesa_IdAndEstadoFalse(mesaId);
    }

    @Override
    protected IGenericRepo<Pedido, String> getRepo() {
        return pedidoRepo;
    }
}

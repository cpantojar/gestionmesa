package com.seven.response;

import java.util.List;

public class FormattedNameResponse {
    List<Error> errors;

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }
}

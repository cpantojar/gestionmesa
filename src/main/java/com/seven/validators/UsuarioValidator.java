package com.seven.validators;

import com.seven.model.Usuario;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class UsuarioValidator implements Validator {
    /**
     * This Validator validates *only* Person instances
     */
    public boolean supports(Class clazz) {
        return UsuarioValidator.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mail", "field.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "usuario", "field.required");
        Usuario request = (Usuario) target;

    }
}

package com.seven.security;

public class AuthRequest {

    private String mail;
    private String password;

    public String getMail() {
        return mail;
    }

    public void setUsername(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

package com.seven.repo;

import com.seven.model.Categoria;
import reactor.core.publisher.Flux;

public interface CategoriaRepo extends IGenericRepo <Categoria, String> {
    Flux<Categoria> findCategoriasByRestaurante_Codigo(String codigo);
    Flux<Categoria> findCategoriasByRestaurante_Ruc(String ruc);
    Flux<Categoria> findCategoriasByRestaurante_Id(String id);
}

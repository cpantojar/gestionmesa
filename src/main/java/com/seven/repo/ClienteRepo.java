package com.seven.repo;

import com.seven.model.Cliente;

public interface ClienteRepo extends IGenericRepo<Cliente, String>{
}

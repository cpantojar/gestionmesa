package com.seven.repo;

import com.seven.model.Mesa;
import org.springframework.data.mongodb.repository.Query;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MesaRepo extends IGenericRepo<Mesa, String> {

    Flux<Mesa> findMesaByRestaurante_Id(String id);
    Flux<Mesa> findMesaByRestaurante_Codigo(String restauranteCodigo);
}

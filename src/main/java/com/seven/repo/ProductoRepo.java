package com.seven.repo;

import com.seven.model.Producto;
import reactor.core.publisher.Flux;

public interface ProductoRepo extends IGenericRepo<Producto, String>{
    Flux<Producto> findProductoByCategoria_Restaurante_Codigo(String codigo);
}

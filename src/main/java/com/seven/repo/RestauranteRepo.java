package com.seven.repo;

import com.seven.model.Restaurante;
import reactor.core.publisher.Mono;

public interface RestauranteRepo extends IGenericRepo<Restaurante, String> {
    Mono<Restaurante> findRestauranteByCodigo(String code);

}

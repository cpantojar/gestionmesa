package com.seven.repo;


import com.seven.model.Preparacion;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

public interface PreparacionRepo extends IGenericRepo<Preparacion, String> {

    Flux<Preparacion> findPreparacionByProducto(String id);

    Mono<Preparacion> findPreparacionByProductoAndDiaPreparacion(String id, LocalDate fechaPreparacion);
}

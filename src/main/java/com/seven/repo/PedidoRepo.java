package com.seven.repo;

import com.seven.model.Pedido;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface PedidoRepo extends IGenericRepo<Pedido, String> {
    Flux<Pedido> findPedidoByRestaurante_Codigo(String codigo);

    Flux<Pedido> findPedidoByRestaurante_Ruc(String ruc);

    Flux<Pedido> findPedidoByRestaurante_Id(String id);

    Mono<Pedido> findPedidoByMesa_IdAndRestaurante_IdAndEstadoIsFalse(String mesaId, String restauranteId);

    Mono<Pedido> findPedidoByMesa_IdAndEstadoFalse(String mesaId);
}

package com.seven.repo;

import com.seven.model.Usuario;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UsuarioRepo extends IGenericRepo<Usuario, String> {
    Mono<Usuario> findUsuarioByMail(String mail);

    Flux<Usuario> findUsuarioByRestaurante_Codigo(String codigo);

    Flux<Usuario> findUsuarioByRestaurante_Ruc(String ruc);

    Flux<Usuario> findUsuarioByRestaurante_Id(String id);
}

package com.seven.repo;

import com.seven.model.Rol;

public interface RolRepo extends IGenericRepo<Rol, String>{
}
